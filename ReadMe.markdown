Pullus System Power Service Manager 17.12.2022
==============================================

This is a Swift framework and a demo application to access the "Root Power Domain IOService" of MacOS.

The application receives the following notifcations:

 - `CanSystemSleep`
 - `SystemWillSleep`
 - `SystemWillPowerOn`
 - `SystemHasPoweredOn`


Requirements
------------

Developed and tested with MacOS 10.12 and Xcode 9.2.

This software was intentionally developed for macOS 10.12 to be able to continue using older systems.
