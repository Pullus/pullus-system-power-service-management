#ifndef _IO_KIT_IO_MESSAGE_H_
#define _IO_KIT_IO_MESSAGE_H_

#import <Foundation/Foundation.h>

// `IORegisterForSystemPower`, …
#include <IOKit/pwr_mgt/IOPMLib.h>

// `kIOMessageCanSystemSleep`, …
#import <IOKit/IOMessage.h>

///
/// Provide the different notifications as an "enum".
///
typedef NS_OPTIONS(UInt32, IOKitIOMessage) {
	IOKitIOMessageCanSystemSleep = kIOMessageCanSystemSleep,
	IOKitIOMessageSystemWillSleep = kIOMessageSystemWillSleep,
	IOKitIOMessageSystemWillPowerOn = kIOMessageSystemWillPowerOn,
	IOKitIOMessageSystemHasPoweredOn = kIOMessageSystemHasPoweredOn
};


#endif
