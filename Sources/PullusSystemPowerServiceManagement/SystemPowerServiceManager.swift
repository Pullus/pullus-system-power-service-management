import IOKit
import os.log


///
/// Dispatches a power notification callback from the
/// "Root Power Domain IOService" to a `SystemPowerServiceManager`
/// (instance method).
///
/// With `IORegisterForSystemPower` only C/Swift-functions can be called
/// -- no closures, methods or delegates. This function is an adapter to use an
/// instance as a receiver.
///
/// To dispatch a callback this function and a `NotificationPort` (as function
/// `refCon`) must be registered with `IOServiceAddMatchingNotification`. This
/// is done by `NotificationPort` itself.
///
/// If the "Root Power Domain IOService" triggers a callback
///  - this function is called and receives the `SystemPowerServiceManager`
///    instance (registered `conRef`) as an argument
///  - this function reinterprets the `conRef` as the
///    `SystemPowerServiceManager` and calls the delegate method.
///
fileprivate
func disptachCallback(refCon: UnsafeMutableRawPointer?, service: io_service_t, messageType: UInt32, messageArgument: UnsafeMutableRawPointer?) {
	// Without a refCon/manager the callback can't be dispatched
	guard let refCon = refCon else {
		os_log("Can't displatch system power service notification. The SystemPowerServiceManager is unknown.", type: .error)
		return
	}
	
	// Restore the `SystemPowerServiceManager` from `refCon`
	let systemPowerServiceManager = Unmanaged<SystemPowerServiceManager>.fromOpaque(refCon).takeUnretainedValue()
	
	// Call delegate instance
	systemPowerServiceManager.handleMatchingCallback(
		refCon: refCon,
		service: service,
		messageType: messageType,
		messageArgument: messageArgument)
}

/// See: Technical Q&A QA1340, Registering and unregistering for sleep and wake notifications
public
class SystemPowerServiceManager {
	
	///
	/// A collection of objects describing a successful registration for power
	/// notifications.
	///
	private
	struct Session {
		
		///
		/// The connection to the "Root Power Domain IOService".
		///
		var ioPMRootDomainConnection: io_connect_t
		
		///
		/// A representative of a successful notification registration.
		///
		/// … a unique notifier which caller must keep and pass to a subsequent call
		/// to `IODeregisterForSystemPower`…
		///
		var notifier: io_object_t
		
		///
		/// The sender of the notification.
		///
		/// "… pointer to an `IONotificationPortRef``, which will deliver the power
		/// notifications…"
		///
		var notificationPort: IONotificationPortRef
	}
	
	///
	/// The registration objects of a successfull registration.
	///
	private
	var session: Session?
	
	///
	/// The run loop that should be used to receive the notifcations.
	///
	private
	let runLoop: RunLoop
	
	// MARK: - Delegate
	
	///
	/// The delegate that receives the notifications.
	///
	public weak
	var delegate: SystemPowerServiceManagerDelegate? {
		willSet { if delegate != nil { unregister() } }
		didSet  { if delegate != nil { register() } }
	}
	
	///
	/// Notifies the delegate.
	///
	/// - NOTE: It is not clear whether a missing `messageArgument` corresponds
	///         with a missing notification ID or with the ID `0`.
	///
	private
	func inform(service: io_service_t, message: IOKitIOMessage, notificationID: intptr_t?) {
		delegate?.handle(
			manager: self,
			service: service,
			message: message,
			notificationID: notificationID)
	}
	
	// MARK: - Life Cycle
	
	///
	/// Creates a new instance that should use the specfic run loop to receive
	/// notifications.
	///
	public
	init(runLoop aRunLoop: RunLoop) {
		runLoop = aRunLoop
	}
	
	deinit {
		unregister()
	}
	
	///
	/// Registers this manger for notifications that are forwared to the
	/// delegate.
	///
	private
	func register() {
		guard session == nil else {
			os_log("Can't register for system power notifications. Already registered.", type: .error)
			return
		}
		
		var notifier: io_object_t = 0
		var notificationPort: IONotificationPortRef?
		
		// Prepare self to use it as a `conRef`
		let selfPointer = Unmanaged.passUnretained(self).toOpaque()
		
		let ioPMRootDomainConnection = IORegisterForSystemPower(selfPointer,
																&notificationPort,
																disptachCallback,
																&notifier)
		
		guard ioPMRootDomainConnection != 0 else {
			os_log("Can't register for system power notifications. No connection to the system power service.", type: .error)
			return
		}
		
		CFRunLoopAddSource(
			runLoop.getCFRunLoop(),
			IONotificationPortGetRunLoopSource(notificationPort).takeUnretainedValue(),
			CFRunLoopMode.commonModes)
		
		session = Session(ioPMRootDomainConnection: ioPMRootDomainConnection,
						  notifier: notifier,
						  notificationPort: notificationPort!)
	}
	
	///
	/// Unregister this insance from notifications.
	///
	private
	func unregister() {
		guard var session = session else {
			os_log("Can't unregister for system power notifications. Not registered.", type: .error)
			return
		}
		
		CFRunLoopRemoveSource(
			runLoop.getCFRunLoop(),
			IONotificationPortGetRunLoopSource(session.notificationPort).takeUnretainedValue(),
			CFRunLoopMode.commonModes)
		
		IODeregisterForSystemPower(&session.notifier)
		
		// "…`IORegisterForSystemPower` … Caller must close return value via
		// `IOServiceClose()`` after calling `IODeregisterForSystemPower` on
		// the notifier argument.…"
		IOServiceClose(session.ioPMRootDomainConnection)
		
		// "…`IORegisterForSystemPower` must be later released by the caller
		// (after calling `IODeregisterForSystemPower`)
		IONotificationPortDestroy(session.notificationPort)
		
		self.session = nil
	}
	
	// MARK: - System Power Service
	
	///
	/// Receives a notification from the `disptachCallback` and forward the
	/// notification to the delegate.
	///
	/// - NOTE: It is not clear whether a missing `messageArgument` corresponds
	///         with a missing notification ID or with the ID `0`.
	///
	fileprivate
	func handleMatchingCallback(refCon: UnsafeMutableRawPointer?,
								service: io_service_t,
								messageType: UInt32,
								messageArgument: UnsafeMutableRawPointer?) {
		
		let message = IOKitIOMessage(rawValue: messageType)
		let notificationID: intptr_t? = messageArgument.map { Int(bitPattern: $0) }

		guard delegate != nil else {
			/// If the delegate is missing `allowPowerChange` must be called for
			/// "CanSystemSleep" and "SystemWillSleep" notifications
			if message == .canSystemSleep || message == .systemWillSleep {
				notificationID.map { allowPowerChange(notificationID: $0) }
			}
			
			return
		}
		
		inform(service: service,
			   message: message,
			   notificationID: notificationID)
	}
	
	///
	/// Notifies the "Root Power Domain IOService" that the power change is
	/// allowed.
	///
	///  - "… Caller **MUST** acknowledge `kIOMessageCanSystemSleep` by calling
	///    `IOAllowPowerChange` or `IOCancelPowerChange`. …"
	///  - "… Callers **MUST** acknowledge this event
	///    (`kIOMessageSystemWillSleep`) by calling `IOAllowPowerChange` …"
	///
	public
	func allowPowerChange(notificationID: intptr_t) {
		guard let session = session else {
			os_log("Can't allow power change. No connection to the system power service.", type: .error)
			return
		}
		
		IOAllowPowerChange(session.ioPMRootDomainConnection, notificationID)
	}
	
	///
	/// Notifies the "Root Power Domain IOService" that the power change should
	/// be canceled.
	///
	/// "… Caller **MUST** acknowledge `kIOMessageCanSystemSleep` by calling
	/// `IOAllowPowerChange` or `IOCancelPowerChange`. …"
	///
	public
	func cancelPowerChange(notificationID: intptr_t) {
		guard let session = session else {
			os_log("Can't cancel power change. No connection to the system power service.", type: .error)
			return
		}
		
		IOCancelPowerChange(session.ioPMRootDomainConnection, notificationID)
	}
	
}
