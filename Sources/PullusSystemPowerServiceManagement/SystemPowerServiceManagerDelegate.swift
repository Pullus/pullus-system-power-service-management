import Foundation

///
/// The protocol to receive power change notification from
/// the `SystemPowerServiceManager`.
///
public
protocol SystemPowerServiceManagerDelegate : class {
	
	///
	/// Is called by the `SystemPowerServiceManager` to notify the delegate
	/// about a power event.
	///
	/// The default implementation dispatches the call to
	/// `handleCanSystemSleep`, `handleSystemWillSleep`,
	/// `handleSystemWillPowerOn` and `handleSystemHasPoweredOn` accordingly.
	///
	///
	/// - NOTE: It is not clear whether a missing ID actually corresponds to the
	///         ID 0. These notifications are **not** dispatched to
	///         `handleCanSystemSleep` and `handleSystemWillSleep`.
	///
	func handle(manager: SystemPowerServiceManager,
				service: io_service_t,
				message: IOKitIOMessage,
				notificationID: intptr_t?)
	
	///
	/// "…  indicates the system is pondering an idle sleep, but gives apps the
	/// chance to veto that sleep attempt. Caller **MUST** acknowledge
	/// `kIOMessageCanSystemSleep` by calling `IOAllowPowerChange` or
	/// `IOCancelPowerChange`. Calling IOAllowPowerChange will not veto the
	/// sleep; any app that calls IOCancelPowerChange will veto the idle sleep.
	/// A kIOMessageCanSystemSleep notification will be followed up to 30
	/// seconds later by a `kIOMessageSystemWillSleep` message. or a
	/// `kIOMessageSystemWillNotSleep` message. …"
	///
	/// The default implementation calls `manager.allowPowerChange`.
	///
	func handleCanSystemSleep(manager: SystemPowerServiceManager, notificationID: intptr_t)
	
	///
	/// "… is delivered at the point the system is initiating a non-abortable
	/// sleep. Callers **MUST** acknowledge this event by calling
	/// `IOAllowPowerChange`. If a caller does not acknowledge the sleep
	/// notification, the sleep will continue anyway after a 30 second timeout
	/// (resulting in bad user experience). Delivered before any hardware is
	/// powered off. …"
	///
	/// The default implementation calls `manager.allowPowerChange`.
	///
	func handleSystemWillSleep(manager: SystemPowerServiceManager, notificationID: intptr_t)
	
	///
	/// "… is delivered at early wakeup time, before most hardware has been
	/// powered on. Be aware that any attempts to access disk, network, the
	/// display, etc. may result in errors or blocking your process until those
	/// resources become available. Caller must **NOT** acknowledge
	/// `kIOMessageSystemWillPowerOn`; the caller must simply return from its
	/// handler. …"
	///
	/// The default implementation does nothing.
	///
	func handleSystemWillPowerOn(manager: SystemPowerServiceManager)
	
	///
	/// "… is delivered at wakeup completion time, after all device drivers and
	/// hardware have handled the wakeup event. Expect this event 1-5 or more
	/// seconds after initiating system wakeup. Caller must **NOT** acknowledge
	/// `kIOMessageSystemHasPoweredOn`; the caller must simply return from its
	/// handler. …"
	///
	/// The default implementation does nothing.
	///
	func handleSystemHasPoweredOn(manager: SystemPowerServiceManager)
	
	///
	/// kIOMessageSystemWillNotSleep
	/// "… is delivered when some app client has vetoed an idle sleep request.
	/// `kIOMessageSystemWillNotSleep` may follow a
	/// `kIOMessageCanSystemSleep` notification, but will not otherwise be sent.
	/// Caller must **NOT** acknowledge `kIOMessageSystemWillNotSleep`; the
	/// caller must simply return from its handler.
	///
	/// Not implemented
}

public
extension SystemPowerServiceManagerDelegate {
	
	///
	/// - NOTE: It is not clear whether a missing ID actually corresponds to the
	///         ID 0.
	///
	func handle(manager: SystemPowerServiceManager,
				service: io_service_t,
				message: IOKitIOMessage,
				notificationID: intptr_t?) {
		switch message {
		case .canSystemSleep:
			guard let notificationID = notificationID else { return }
			handleCanSystemSleep(manager: manager, notificationID: notificationID)

		case .systemWillSleep:
			guard let notificationID = notificationID else { return }
			handleSystemWillSleep(manager: manager, notificationID: notificationID)

		case .systemWillPowerOn:
			handleSystemWillPowerOn(manager: manager)
			
		case .systemHasPoweredOn:
			handleSystemHasPoweredOn(manager: manager)
		
		default:
			break
		}
	}
	
	func handleCanSystemSleep(manager: SystemPowerServiceManager, notificationID: intptr_t) {
		manager.allowPowerChange(notificationID: notificationID)
	}
	
	func handleSystemWillSleep(manager: SystemPowerServiceManager, notificationID: intptr_t) {
		manager.allowPowerChange(notificationID: notificationID)
	}
	
	func handleSystemWillPowerOn(manager: SystemPowerServiceManager) {
	}
	
	func handleSystemHasPoweredOn(manager: SystemPowerServiceManager) {
	}
	
}
