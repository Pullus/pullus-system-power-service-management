import Foundation

import PullusSystemPowerServiceManagement

class Main : SystemPowerServiceManagerDelegate {
	
	let powerService = SystemPowerServiceManager(runLoop: .main)
	
	func handleCanSystemSleep(manager: SystemPowerServiceManager, notificationID: intptr_t) {
		print("Can sleep")
		manager.allowPowerChange(notificationID: notificationID)
	}
	
	func handleSystemWillSleep(manager: SystemPowerServiceManager, notificationID: intptr_t) {
		print("Will sleep")
		manager.allowPowerChange(notificationID: notificationID)
	}
	
	func handleSystemWillPowerOn(manager: SystemPowerServiceManager) {
		print("Will power on")
	}
	
	func handleSystemHasPoweredOn(manager: SystemPowerServiceManager) {
		print("Has powered on")
	}
	
	func run() {
		powerService.delegate = self

		RunLoop.main.run()
	}
	
}

Main().run()
